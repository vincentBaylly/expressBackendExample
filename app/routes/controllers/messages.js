const Message = require("../../model/message.class");

const router = require("express").Router();

router.get("/lists", (req, res) => {
  //TODO - retourne une liste de messages
});

router.post("/add", (req, res) => {
  console.log("post method " + JSON.stringify(req.body));
  const myMessage = new Message();

  myMessage
    .create(req.body)
    .then((message) => {
      res.json(message);
    })
    .catch((error) => {
      res.status(500);
      res.json(error);
    });
});

module.exports = router;
