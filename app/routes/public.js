const routes = require("express")();

//enregistre nos routes d'authentification avec Express.
routes.use("/message", require("./controllers/messages"));

module.exports = routes;
