//import de librairie
const express = require("express");
const cors = require("cors");
const passport = require("passport");

//TODO - Externaliser la configuration avec Webpack
//configuration
const port = 3000;

const routes = express();

/* https://developer.mozilla.org/fr/docs/Web/HTTP/CORS
 * Le «  Cross-origin resource sharing » (CORS) ou « partage des ressources entre origines multiples »
 * (en français, moins usité) est un mécanisme qui consiste à ajouter des en-têtes HTTP
 * afin de permettre à un agent utilisateur d'accéder à des ressources d'un serveur situé
 * sur une autre origine que le site courant. Un agent utilisateur réalise
 * une requête HTTP multi-origine (cross-origin) lorsqu'il demande une ressource provenant d'un domaine,
 * d'un protocole ou d'un port différent de ceux utilisés pour la page courante.
 */
routes.use(cors());

routes.use(express.urlencoded({ extended: true }));
routes.use(express.json()); // To parse the incoming requests with JSON payloads

// Écouter sur le port défini plus haut
routes.listen(port, (err) => {
  if (err) console.error(err);
  console.log(`Listening for Requests on port: ${port}`);
});

// Page index.html du serveur lors de la requête à la racine
routes.get("/", function (req, res, next) {
  res.sendfile("./public/index.html");
});

//ajout des services publics qui ne necessite pas d'authentification
routes.use("/api/public", require("./app/routes/public"));
//ajout des services privés qui necessite un token jwt pour etre appelé
routes.use(
  "/api/private",
  passport.authenticate("jwt", { session: false }),
  require("./app/routes/private")
);
