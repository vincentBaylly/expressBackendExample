# Backend Example

API template to understand mechanism of backends services

## Installation

#### Node.js

https://nodejs.org/en/

## Starting and Run Application

- Download and install application dependencies

```bash
npm install
```

- Start Application

```bash
node server.js
```

## ENV file

Create your own environment file to declare your local variable.

See the file ENV.md for list of existing variable to add to your local file.  
Add the documentation in

## dotenv

https://github.com/motdotla/dotenv#readme

## Passport

http://www.passportjs.org/docs/

## Application Architecture

```
.
├── config                  # App configuration files
│   └── ...                 # Other configurations
├── routes
│   ├── controllers         # Request managers
│   └── routes.js           # Define routes and middlewares here
├── models              # Models
├── core                    # Business logic implementation
│   └── ...                 # Other business logic implementations
├── utils                   # Util libs (formats, validation, etc)
├── package.json
├── README.md
└── server.js                  # API starting point
```

> Vue.js

## Build Setup

```bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

## License

[Apache 2.0 License](https://img.shields.io/crates/l/:crate.svg?style=flat)
